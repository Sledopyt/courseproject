import datetime
import pandas as pd
import sqlalchemy as sa
import sys
from PyQt5 import QtWidgets, QtGui, QtCore, QtSql
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker

Base = declarative_base()

class Packings(Base):
    __tablename__ = "Packing"

    ID = sa.Column(sa.Integer, primary_key=True)
    DeliveryNo = sa.Column(sa.Integer, nullable=False)
    CartonID = sa.Column(sa.String(10))
    Pallet = sa.Column(sa.String(10))
    Item = sa.Column(sa.String(10))
    Manufacturer = sa.Column(sa.String(255))
    CountryOfOriginCode = sa.Column(sa.String(3))
    SizeDescription = sa.Column(sa.String(50))
    Years = sa.Column(sa.String(10))
    Months = sa.Column(sa.String(10))
    Other = sa.Column(sa.String(10))
    Description = sa.Column(sa.String(255))
    Gender = sa.Column(sa.String(50))
    LocalDescription = sa.Column(sa.String(255))
    Composition = sa.Column(sa.String(255))
    Knitted_Woven = sa.Column(sa.String(50))
    ClassificationGroup = sa.Column(sa.String(50))
    UKVatRate = sa.Column(sa.String(50))
    CustomsProcedureCode = sa.Column(sa.String(50))
    UKCommodityCode = sa.Column(sa.String(50))
    IntlCommodityCode = sa.Column(sa.String(50))
    IntlCommodityCode2 = sa.Column(sa.String(50))
    IntlCommodityCode3 = sa.Column(sa.String(50))
    IntlCommodityCode4 = sa.Column(sa.String(50))
    IntlCommodityCode5 = sa.Column(sa.String(50))
    CartonWeight = sa.Column(sa.String(50))
    Qty = sa.Column(sa.Integer)
    PackQty = sa.Column(sa.Integer)
    TransferPriceGBP = sa.Column(sa.Float)
    TransferValueGBP = sa.Column(sa.Float)
    TransferPriceRUB = sa.Column(sa.Float)
    TransferValueRUB = sa.Column(sa.Float)
    Weight = sa.Column(sa.String(10))
    Height = sa.Column(sa.String(10))
    Chest = sa.Column(sa.String(10))
    Waist = sa.Column(sa.String(10))
    Hips = sa.Column(sa.String(10))
    HeadSize = sa.Column(sa.String(10))
    Weight2 = sa.Column(sa.String(10))
    Lenght = sa.Column(sa.String(10))
    ShoeLenght = sa.Column(sa.String(10))
    SockLenght = sa.Column(sa.String(10))
    GlovesLenght = sa.Column(sa.String(10))
    NeckGirth = sa.Column(sa.String(10))
    DMCode = sa.Column(sa.String(10))
    Result = sa.Column(sa.Text)


class Base_NEXTS(Base):
    __tablename__ = "Base_NEXT"

    ID = sa.Column(sa.Integer, primary_key=True)
    Code = sa.Column(sa.Integer)
    Item = sa.Column(sa.String(10))
    ItemDescription = sa.Column(sa.String(255))
    TranslatedProductName = sa.Column(sa.String(255))
    Gender = sa.Column(sa.String(50))
    CodTNVED_Masha = sa.Column(sa.String(20))
    CodTNVED_Zhenya = sa.Column(sa.String(20))
    ItemName_Zhenya = sa.Column(sa.String(255))
    field9 = sa.Column(sa.String(50))
    ItemTypeTranslated = sa.Column(sa.String(255))


class Dict(Base):
    __tablename__ = "Словарь"

    ID = sa.Column(sa.Integer, primary_key=True)
    Eng = sa.Column(sa.String(255))
    Rus = sa.Column(sa.String(255))
    Priority = sa.Column(sa.Integer)


class Historys(Base):
    __tablename__ = "History"

    ID = sa.Column(sa.Integer, primary_key=True)
    Code = sa.Column(sa.Integer)
    Item = sa.Column(sa.String(10))
    Manufacturer = sa.Column(sa.String(255))
    Country_code = sa.Column(sa.String(3))
    NDS = sa.Column(sa.Integer)
    TNVED_Code = sa.Column(sa.String(20))
    TraditionName = sa.Column(sa.String(255))
    NameGroup31 = sa.Column(sa.String(255))
    Qty = sa.Column(sa.Integer)
    PackQty = sa.Column(sa.Integer)
    Price = sa.Column(sa.Float)
    FacturaPrice = sa.Column(sa.String(10))
    MoneyCode = sa.Column(sa.Integer)
    EdIzmCode = sa.Column(sa.Integer)
    Netto = sa.Column(sa.String(10))
    MNRCode = sa.Column(sa.Integer)
    SS_DS = sa.Column(sa.String(255))
    SS_DataStart = sa.Column(sa.DateTime)
    SS_DataEnd = sa.Column(sa.DateTime)
    SGRCode = sa.Column(sa.String(10))
    SGR = sa.Column(sa.String(255))
    SGRData = sa.Column(sa.DateTime)
    Invoice = sa.Column(sa.Integer)
    InvoiceData = sa.Column(sa.DateTime)
    Trademark = sa.Column(sa.String(255))
    Mark = sa.Column(sa.String(255))
    Model = sa.Column(sa.String(255))
    DMCode = sa.Column(sa.String(255))


class MainWindow(QtWidgets.QMainWindow):
    def __init__(self):
        super().__init__()
        self.setGeometry(40, 100, 1200, 650)
        self.font = QtGui.QFont("Nimes New Roman", 15)

        self.label = QtWidgets.QLabel(self)    # Метка
        self.label.setGeometry(420, 100, 200, 100)
        self.label.setText("Обработка файлов NEXT")
        self.label.setStyleSheet("font-size: 30px")
        self.label.adjustSize()           

        self.load_button = QtWidgets.QPushButton(self)
        self.load_button.setText("Загрузка нового инвойса")
        self.load_button.setGeometry(150, 280, 350, 50)
        self.load_button.setFont(self.font)        

        self.unload_button = QtWidgets.QPushButton(self)
        self.unload_button.setText("Просмотр сохраненного инвойса")
        self.unload_button.setGeometry(700, 280, 350, 50)
        self.unload_button.setFont(self.font)

        self.invoice_combobox = QtWidgets.QComboBox(self)
        df = pd.read_sql("select distinct Invoice from History order by Invoice", engine)
        for row in df.itertuples():
            self.invoice_combobox.addItem(str(row[1]), row[1])
        self.invoice_combobox.setGeometry(700, 340, 350, 40)
        self.invoice_combobox.setFont(self.font)        

        self.unload_button.clicked.connect(self.unload_invoice)
        self.load_button.clicked.connect(self.load_invoice)

    def unload_invoice(self):
        value = self.invoice_combobox.itemData(self.invoice_combobox.currentIndex())
        tw = DBWindow(value)
        tw.show()

    def load_invoice(self):
        xf = QtWidgets.QFileDialog.getOpenFileName()[0]
        df = pd.read_excel(xf, sheet_name='Packing List')
        query = "DELETE FROM Packing"
        engine.execute(query)
        for row in df.itertuples():
            values = (
                {"v1" : row[1],
                "v2" : row[2],
                "v3" : row[3],
                "v4" : row[4],
                "v5" : row[5],
                "v6" : row[6],
                "v7" : row[7],
                "v8" : row[8],
                "v9" : row[9],
                "v10" : row[10],
                "v11" : row[11],
                "v12" : row[12],
                "v13" : row[13],
                "v14" : row[14],
                "v15" : row[15],
                "v16" : row[16],
                "v17" : row[17],
                "v18" : row[18],
                "v19" : row[19],
                "v20" : row[20],
                "v21" : row[26],
                "v22" : row[27],
                "v23" : row[30],}
            )
            query = """
            INSERT INTO Packing (DeliveryNo, CartonID, Pallet, Item, Manufacturer, CountryOfOriginCode,
            SizeDescription, Years, Months, Other, Description, Gender, LocalDescription, Composition,
            Knitted_Woven, ClassificationGroup, UKVatRate, CustomsProcedureCode, UKCommodityCode,
            IntlCommodityCode, Qty, PackQty, TransferPriceRUB) 
            VALUES (@v1, @v2, @v3, @v4, @v5, @v6, @v7, @v8, @v9, @v10, @v11, @v12, @v13, @v14,
            @v15, @v16, @v17, @v18, @v19, @v20, @v21, @v22, @v23)
            """
            engine.execute(query, values)
        tw = LoadFileWindow()
        tw.show()


class DBWindow(QtWidgets.QMainWindow):
    def __init__(self, invnum):
        super().__init__()
        self.setGeometry(0, 0, 1200, 650)
        self.font = QtGui.QFont("Nimes New Roman", 15)
        self.setParent(main_window)
        self.myfilter = invnum

        engine.connect()
        self.table = QtWidgets.QTableView(self)
        self.model = SqlAlchemyTableModel(session, Historys, 
        [('ID', Historys.ID, "ID", {"editable": False} ),
        ('Артикул', Historys.Item, "Item", {"editable": False} ),
        ('Производитель', Historys.Manufacturer, "Manufacturer", {"editable": False} ),
        ('Код страны происхождения', Historys.Country_code, "Country_code", {"editable": True} ),
        ('Код ТН ВЭД', Historys.TNVED_Code, "TNVED_Code", {"editable": True} ),
        ('Наименование', Historys.TraditionName, "TraditionName", {"editable": True} ),
        ('Наименование для 31 графы', Historys.NameGroup31, "NameGroup31", {"editable": True} ),
        ('Кол-во', Historys.Qty, "Qty", {"editable": True} ),
        ('Кол-во в упаковке', Historys.PackQty, "PackQty", {"editable": True} ),
        ('Цена', Historys.Price, "Price", {"editable": True} ),
        ('Счет', Historys.Invoice, "Invoice", {"editable": True} ),])
        self.model.setFilter(sa.text("Invoice=="+str(self.myfilter)))
        self.table.setModel(self.model)
        self.table.hideColumn(0)
        self.table.setGeometry(0, 0, 1200, 550)
        self.table.show()

        self.save_button = QtWidgets.QPushButton(self)
        self.save_button.setText("Сохранить в Excel")
        self.save_button.setGeometry(100, 610, 150, 30)

        self.close_button = QtWidgets.QPushButton(self)
        self.close_button.setText("Закрыть")
        self.close_button.setGeometry(520, 610, 150, 30)

        self.save_button.clicked.connect(self.save_to_file)
        self.close_button.clicked.connect(self.close_form)
 
    def close_form(self):
        self.close()

    def save_to_file(self):
        xf = QtWidgets.QFileDialog.getSaveFileName()[0]
        df = pd.read_sql(
            "SELECT Item, Manufacturer, Country_Code, TNVED_Code, TraditionName, \
            NameGroup31, Qty, PackQty, Price, Invoice FROM History \
            WHERE Invoice=" + str(self.myfilter) + " order by ID",
            engine
            )
        df.to_excel(
            xf, 
            sheet_name="Выгрузка",
            header=(
                    "Артикул",
                    "Производитель",
                    "Страна", 
                    "Код товара", 
                    "Традиционное наименование товара", 
                    "Наименование группы в гр.31",
                    "Кол-во",
                    "PackQty",
                    "Сумма",
                    "Инвойс",
                    )
            )
        QtWidgets.QMessageBox.information(None, 'Информация', 'Файл сохранен')


class LoadFileWindow(QtWidgets.QMainWindow):
    def __init__(self):
        super().__init__()
        self.setGeometry(0, 0, 1200, 650)
        self.setParent(main_window)

        engine.connect()
        self.table = QtWidgets.QTableView(self)
        self.model = SqlAlchemyTableModel(session, Packings, 
        [('ID', Packings.ID, "ID", {"editable": False} ),
        ('Item', Packings.Item, "Item", {"editable": False} ),
        ('Производитель', Packings.Manufacturer, "Manufacturer", {"editable": False} ),
        ('Код страны происхождения', Packings.CountryOfOriginCode, "CountryOfOriginCode", {"editable": True} ),
        ('Описание размера', Packings.SizeDescription, "SizeDescription", {"editable": True} ),
        ('Лет', Packings.Years, "Years", {"editable": True} ),
        ('Месяцев', Packings.Months, "Months", {"editable": True} ),
        ('Код ТН ВЭД', Packings.UKCommodityCode, "UKCommodityCode", {"editable": True} ),
        ('Описание', Packings.Description, "Description", {"editable": True} ),
        ('Gender', Packings.Gender, "Gender", {"editable": True} ),
        ('Описание свое', Packings.LocalDescription, "LocalDescription", {"editable": True} ),
        ('Состав', Packings.Composition, "Composition", {"editable": True} ),])
        self.table.setModel(self.model)
        self.table.hideColumn(0)
        self.table.setSortingEnabled(True)
        self.table.sortByColumn(0, QtCore.Qt.AscendingOrder)

        self.table.setGeometry(0, 0, 1200, 540)
        
        self.table.show()

        self.label = QtWidgets.QLabel(self)    # Метка
        self.label.setGeometry(50, 550, 200, 100)
        self.label.setText("Пошаговая обработка (дождаться сообщения об окончании!)")
        self.label.setStyleSheet("font-size: 15px")
        self.label.adjustSize()        
        
        self.gender_button = QtWidgets.QPushButton(self)
        self.gender_button.setText("Обновить Gender")
        self.gender_button.setGeometry(25, 580, 150, 50)

        self.composition_button = QtWidgets.QPushButton(self)
        self.composition_button.setText("Перевод состава")
        self.composition_button.setGeometry(190, 580, 150, 50)
        self.composition_button.setEnabled(False)

        self.final_button = QtWidgets.QPushButton(self)
        self.final_button.setText("Финальная обработка")
        self.final_button.setGeometry(355, 580, 150, 50)
        self.final_button.setEnabled(False)

        self.save_button = QtWidgets.QPushButton(self)
        self.save_button.setText("Сохранить в БД")
        self.save_button.setGeometry(785, 580, 150, 50)
        self.save_button.setEnabled(False)
        
        self.close_button = QtWidgets.QPushButton(self)
        self.close_button.setText("Закрыть без сохранения")
        self.close_button.setGeometry(950, 580, 150, 50)
        
        self.save_button.clicked.connect(self.save_to_base)
        self.close_button.clicked.connect(self.close_form)
        self.gender_button.clicked.connect(self.gender_update)
        self.composition_button.clicked.connect(self.composition_update)
        self.final_button.clicked.connect(self.final_update)

    def close_form(self):
        self.close()

    def save_to_base(self):
        query = """
        INSERT INTO History(Item, Manufacturer, Country_code, TNVED_Code, TraditionName, NameGroup31, Qty, PackQty, Price, Invoice, Code)
        SELECT Item, Manufacturer, CountryOfOriginCode, UKCommodityCode, LocalDescription, Item || ', состав: ' || Composition, Qty, PackQty, TransferPriceRUB, DeliveryNo, CAST(substr(Item,1,6) as int)
        FROM Packing
        """
        engine.execute(query)
        QtWidgets.QMessageBox.information(None, 'Информация', 'Сохранение завершено')
        self.close()

    def gender_update(self):
        query = """
        UPDATE Packing
	    SET Gender = (select Gender from Base_NEXT fb where CAST(substr(Packing.Item,1,6) as int) = fb.Code order by ID LIMIT 1)
	    where ifnull(Gender, '') = ''
        """
        engine.execute(query)
        QtWidgets.QMessageBox.information(None, 'Информация', 'Обновление завершено')
        self.repaint()
        self.composition_button.setEnabled(True)

    def composition_update(self):
        row = self.model.rowCount(self)
        for x in range(row):
            self.model.setData(self.model.index(x, 11), composition_transfer(self.table.model().index(x, 11).data()))
        QtWidgets.QMessageBox.information(None, 'Информация', 'Обновление завершено')
        self.repaint()
        self.final_button.setEnabled(True)

    def final_update(self):
        row = self.model.rowCount(self)
        for x in range(row):
            self.model.setData(
                self.model.index(x, 10), 
                packing_treatment(self.table.model().index(x, 1).data(), self.table.model().index(x, 9).data())
                )
            self.model.setData(
                self.model.index(x, 7), 
                ved_treatment(self.table.model().index(x, 1).data(), self.table.model().index(x, 7).data())
                )
        QtWidgets.QMessageBox.information(None, 'Информация', 'Обновление завершено')
        self.repaint()
        self.save_button.setEnabled(True)


class SqlAlchemyTableModel(QtSql.QSqlTableModel):
    # Класс отсюда : https://gist.github.com/nerdoc/6815e16559e5f7e5894daf85c4e50128
    """A Qt Table Model that binds to a SQLAlchemy query
    Example:
    >>> model = AlchemicalTableModel(Session,
            Entity, 
            [('Name', Entity.name, "name", {"editable": True} )])
    >>> table = QTableView()
    >>> table.setModel(model)
    """

    def __init__(self, session, entity, columns):
        """Constructor for the model.
        
        @param session: The SQLAlchemy session object.
        @param entity: The entity class that represents the SQLAlchemy data object 
        @param columns: A list of column 4-tuples
          (header, sqlalchemy column, column name, extra parameters as dict)
          if the sqlalchemy column object is 'Entity.name', then column name
          should probably be 'name'.
          'Entity.name' is what will be used when setting data and sorting,
          'name' will be used to retrieve the data.
        """
        
        super().__init__()
        #TODO self.sort_data = None
        self.session = session
        self.fields = columns
        self.query = session.query
        self.entity = entity

        self.results = None
        self.count = None
        self.sort = None
        self.filter = None

        self.refresh()

    def headerData(self, col, orientation, role):
        if orientation == QtCore.Qt.Horizontal and role == QtCore.Qt.DisplayRole:
            return QtCore.QVariant(self.fields[col][0])
        return QtCore.QVariant()

    def setFilter(self, newFilter):
        """Sets or clears the newFilter.
        
        Clear the filter by setting newFilter to None
        """
        self.filter = newFilter
        self.refresh()

    def refresh(self):
        """Recalculates self.results and self.count"""

        self.layoutAboutToBeChanged.emit()

        #q = self.session.query
        
        if self.sort is not None:
            order, col = self.sort
            col = self.fields[col][1]
            if order == QtCore.Qt.DescendingOrder:
                col = col.desc()
        else:
            col = None
            
        if self.filter is not None:
            #q = q.filter(self.filter)
            self.results = self.session.query(self.entity).filter(self.filter).all()
        else:
            self.results = self.session.query(self.entity).all()
        
        #self.results = q(self.entity).all()
        self.count = len(self.results)
        self.layoutChanged.emit()

    def flags(self, index):
        _flags = QtCore.Qt.ItemIsEnabled | QtCore.Qt.ItemIsSelectable

        if self.sort is not None:
            order, col = self.sort

            if self.fields[col][3].get('dnd', False) and index.column() == col:

                _flags |= QtCore.Qt.ItemIsDragEnabled | QtCore.Qt.ItemIsDropEnabled

        if self.fields[index.column()][3].get('editable', False):
            _flags |= QtCore.Qt.ItemIsEditable

        return _flags

    def supportedDropActions(self):
        return QtCore.Qt.MoveAction

    def dropMimeData(self, data, action, row, col, parent):
        if action != QtCore.Qt.MoveAction:
            return

        return False

    def rowCount(self, parent):
        return self.count or 0

    def columnCount(self, parent):
        return len(self.fields)

    def data(self, index, role):
        if not index.isValid():
            return QtCore.QVariant()

        elif role not in (QtCore.Qt.DisplayRole, QtCore.Qt.EditRole):
            return QtCore.QVariant()

        row = self.results[index.row()]
        name = self.fields[index.column()][2]

        return getattr(row, name)

    def setData(self, index, value, role=None):
        row = self.results[index.row()]
        name = self.fields[index.column()][2]

        try:
            #setattr(row, name, value.toString())
            setattr(row, name, value)
            self.session.commit()
        except Exception as ex:
            # FIXME: data layer should not open GUI Messagebox!
            QtWidgets.QMessageBox.critical(None, 'SQL Error', str(ex))
            return False
        else:
            self.dataChanged.emit(index, index)
            return True

    def sort(self, col, order):
        """Sort table by given column number."""
        if self.sort != (order, col):
            self.sort = order, col
            self.refresh()                


def gender_text(gender):
    gender_string = ""
    if gender == "Baby Boy and Girls":
        gender_string = "для новорожденных"
    elif gender == "Boys":
        gender_string = "для мальчиков"
    elif gender == "Girls":
        gender_string = "для девочек"
    elif gender == "Mens":
        gender_string = "для взрослых (муж.)"
    elif gender == "Womens":
        gender_string = "для взрослых (жен.)"
    return gender_string


def composition_transfer(composition):
    new_composition = composition
    df = pd.read_sql("select Eng, Rus from Словарь order by Priority", engine)
    for row in df.itertuples():
        new_composition = new_composition.replace(row[1], row[2])
    return new_composition


def packing_treatment(item, gender):
    full_desc = ""
    query = "select * from Base_NEXT where Code = " + item[:6] + " order by ID DESC LIMIT 1"
    df = pd.read_sql(query, engine)
    if df.empty == False:
        for row in df.itertuples():    
            print(row[11])
            if row[11] == None:
                new_desc = "(нет данных)"
            else:
                new_desc = row[11]
            gender_result = gender_text(gender)
            full_desc = new_desc + " " + gender_result

    return full_desc


def ved_treatment(item, tnved):
    query = "select * from Base_NEXT where Code = " + item[:6] + " order by ID DESC LIMIT 1"
    df = pd.read_sql(query, engine)
    if df.empty == False:
        for row in df.itertuples():    
            if row[7]!="" and row[7]!=tnved:
                print(item, tnved, row[7])
                return row[7]

    return tnved
 
engine = sa.create_engine("sqlite:///next2020.sqlite")
Base.metadata.create_all(engine)
Session = sessionmaker()
session = Session(bind=engine)

app = QtWidgets.QApplication(sys.argv)

main_window = MainWindow()
main_window.setWindowTitle("NEXT")

main_window.show()

sys.exit(app.exec_())